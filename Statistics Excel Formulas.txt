



  =QUARTILE(   ,1)          1st Quartile

  =QUARTILE(    ,3)          3rd Quartile

   =(       )                IQR
   
   =STDDEV.S( )              Standard Deviation sample

   =STDEV.P(  )              Standard Deviation Population

   =(  )/                    Mean

   =(   +1.5*  )             Upper Fence

   =(    - 1.5*  )           Lower Fence